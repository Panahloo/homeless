﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float Speed = 10;

    float moveHorizontal;
    float moveVertical;
    Rigidbody rigidBody;

    // Start is called before the first frame update
    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void FixedUpdate()
    {
        moveHorizontal = Input.GetAxis("Horizontal");
        moveVertical = Input.GetAxis("Vertical");

        //rigidBody.AddForce(new Vector3(moveHorizontal, 0.0f, moveVertical) * Speed);
        rigidBody.velocity += new Vector3(moveHorizontal, 0, moveVertical) * Speed;
    }
}
